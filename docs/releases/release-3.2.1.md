# Release 3.2.1

## Überblick

Im Mittelpunkt des Releases 3.2.1 steht die Bereitstellung der neuen Statistikseite für User, die Möglichkeit zum Download der Daten aus der Statistik und die damit verbundenen Anpassungen an der Suche. Des Weiteren wurde das neue Reporting Template für November 2018 integriert.

## Features

### Aktualisierung des Reporting-Templates

!!! info

    User

Das Reporting-Template[^1] für November 2018 wurde gemäß der neuen Vorlage aktualisiert. Die folgenden Änderungen wurden umgesetzt.

![Visual Studio Code](../images/code.png)

!!! warning "Installation on macOS"

    When you're running the pre-installed version of Python on macOS, `pip`
    tries to install packages in a folder for which your user might not have
    the adequate permissions. There are two possible solutions for this:

    1. **Installing in user space** (recommended): Provide the `--user` flag
      to the install command and `pip` will install the package in a user-site
      location. This is the recommended way.

    2. **Switching to a homebrewed Python**: Upgrade your Python installation
      to a self-contained solution by installing Python with Homebrew. This
      should eliminate a lot of problems you may be having with `pip`.

|Titel|Aktualisierung des Reporting-Templates|
|-|-|
|Beschreibung|Das Reporting-Template für November 2018 wurde gemäß der neuen Vorlage aktualisiert. Die folgenden Änderungen wurden umgesetzt|
|Zielgruppe|User|

## Bugfixes

Folgende Bugfixes wurden in Release 3.2.1 ausgeliefert:

* Bugfix 1
* Bugfix 2

[^1]: This is a footnote.